export enum CredentialTypes {
  participant = 'Participant',
  service_offering = 'Service Offering (experimental)',
  common = 'Common'
}

export enum SelfDescriptionTypes {
  PARTICIPANT = 'LegalParticipant',
  LEGAL_REGISTRATION_NUMBER = 'legalRegistrationNumber',
  SERVICE_OFFERING = 'ServiceOfferingExperimental'
}
